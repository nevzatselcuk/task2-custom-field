import Vue from 'vue'
import App from './App.vue'
import store from './store'

/**
 * AXIOS VUE
 */
//AXIOS
import axios from 'axios';
axios.defaults.baseURL = 'http://localhost:3000';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, axios);

/**
 * BOOTSTARP VUE
 */
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
/**
 * PLUGINS
 */
require('./plugins/Plugins');
require('./plugins/Fontawesome');
require('./plugins/Prototype');
require('./plugins/Validation');

import './assets/style.css'
import 'sweetalert2/dist/sweetalert2.min.css'


import VueSweetalert2 from 'vue-sweetalert2'
Vue.use(VueSweetalert2)

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
