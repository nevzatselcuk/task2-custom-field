export const AlertMixin = {
    methods: {
        async popAlert(type, title, message, timer) {
            let stitle, stimer;
            switch (type) {
                case "success":
                    stitle = 'Success';
                    stimer = 1500;
                    break;
                case "error":
                    stitle = 'Error!!!';
                    stimer = 2500;
                    break;
                case "info":
                    stitle = 'Info';
                    stimer = 1500;
                    break;
                case "question":
                    stitle = 'Do you confirm?';
                    break;
                default:
                    stitle = 'Warning';
                    break;
            }
            let msg = message && typeof message == 'string' ? message.replace("\r\n", "<br/>") : 'No Message'
            let swalOptions = {
                html: msg,
                position: "top-end",
                icon: type,
                title: typeof title != "undefined" ? title : stitle,
                showConfirmButton: true,
                timer: typeof timer != "undefined" ? timer : stimer,
                confirmButtonText: "Ok",
                buttonsStyling: false,
                customClass : {
                    confirmButton: "btn btn-block btn-success"
                }

            }
            this.$swal(swalOptions);
        },
        popConfirm(title, message) {
            let comp = this;
            return new Promise(function(resolve) {
                comp
                    .$swal({
                        title: title.replace("\r\n", "<br/>"),
                        position: "top-end",
                        html:
                            typeof message != "undefined" ? message : 'Do you confirm?',
                        icon:'question',
                        showCancelButton: true,
                        cancelButtonText: 'Cancel',
                        buttonsStyling: false,
                        customClass : {
                            confirmButton : "btn btn-success mr-3 w-25",
                            cancelButton : "btn btn-danger w-25"
                        }

                    })
                    .then(result => {
                        if (result.value) {
                            resolve();
                        }
                    });
            });
        },
    },
}
