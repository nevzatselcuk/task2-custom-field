import Vue from "vue";
import VeeValidate from "vee-validate";

import(`vee-validate/dist/locale/en`).then(locale => {
    VeeValidate.Validator.localize(locale.name, locale);
});

const config = {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    errorBagName: "veeErrors",
    fieldsBagName: "veeFields",
    events: "input|blur",
    locale: 'en',
    inject: true,
    validity: false,
    useConstraintAttrs: true
}


Vue.use(VeeValidate, config);
