import Vue from 'vue';

import VueSelect from "vue-select";
import 'vue-select/dist/vue-select.css'
Vue.component('VueSelect', VueSelect)

import VueDatepicker from "vuejs-datepicker/dist/vuejs-datepicker.esm.js";
Vue.component('VueDatepicker', VueDatepicker)

import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';
Vue.component('DatePicker', DatePicker)




